import os


def main():
    """
    main accepts an integer from the user and prints the square of all the integers up to the input integer
    :return:
    """
    n = int(os.environ["num"])
    for i in range(n):
        print(str(i) + ' ' + str(i*i))


if __name__ == '__main__':
    main()
