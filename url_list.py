import requests
import os
    
    
def main():
    """
    main calls get_url and prints the HTML of each url
    return None
    """
    some_str = os.environ["some_str"]
    url_list = some_str.split(',')
    for site in url_list:
        s = requests.get(site)
        print(s.text)
        print()
        print()
        
        
if __name__ == '__main__':
    main()

