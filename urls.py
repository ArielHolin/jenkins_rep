import os


def main():
    """
    main open 5 urls from a file containing them
    return: None
    """
    file_path = os.environ["file_path"]
    
    try:
        with open(file_path, 'r') as f:
            url_list = f.read().split(',')
    except IOError as ioe:
        print(ioe)
    except NameError as ne:
        print(ne)      

print(url_list)


if __name__ == '__main__':
    main()